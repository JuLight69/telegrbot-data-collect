import requests
import telebot
import face_recognition
import os
import librosa
import ffmpeg
import subprocess
import BotSql
from datetime import datetime


#token
bot = telebot.TeleBot(token)
meanU = f'https://api.telegram.org/bot{token}'

@bot.message_handler(commands=['start','hello'])
def start_handler(message):
    session = BotSql.actBase()
    name = message.chat.first_name
    try:
        BotSql.appendBase(session, message.chat.id, name)
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    bot.send_message(message.chat.id, f'''Здравствуй, {name}! Я могу сохранить твои голосовые сообщения,
                    а также фотографии, на которых есть лица! Чтобы удалить все сохраненные данные используй команду /forgetme ''')
    BotSql.close(session)

@bot.message_handler(commands=['forgetme'])
def start_handler(message):
    name = message.chat.first_name
    session = BotSql.actBase()
    BotSql.deleteBase(session, message.chat.id, name)
    bot.send_message(message.chat.id, 'Все ваши данные успешно удалены из базы.')
    BotSql.close(session)

@bot.message_handler(content_types=['photo'])
def text_handler(message):
    path = os.getcwd()
    name = message.chat.first_name
    session = BotSql.actBase()
    chat_id = message.chat.id
    idp = message.photo[0].file_id
    if idp != None:
        i = BotSql.countPhoto(session, chat_id, name)
        name = f'photo_{i}.png'
        path = os.getcwd() + f'\\users\\{chat_id}\\photo\\{name}'
        file_infop = bot.get_file(idp)
        downloaded_file = bot.download_file(file_infop.file_path)
        try:
            with open(path,'wb') as new_file:
                new_file.write(downloaded_file)
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{name}: ', err, f' :{datetime.today()}', file=f)
        image = face_recognition.load_image_file(path)
        face_locations = face_recognition.face_locations(image)
        if len(face_locations):
            BotSql.appendBasePhoto(session, chat_id, path, name)
            bot.send_message(chat_id, 'Красиво, пожалуй я сохраню это')
        else:
            bot.send_message(chat_id, 'Красиво, жаль лица не видно..')
            os.remove(path)
            BotSql.close(session)

@bot.message_handler(content_types=['voice'])
def text_handler(message):
    path = os.getcwd()
    name = message.chat.first_name
    session = BotSql.actBase()
    chat_id = message.chat.id
    id = message.voice.file_id
    if id != None:
        i = BotSql.countVoice(session, chat_id, name)
        name = f'audio_message_{i}'
        path = os.getcwd() + f'\\users\\{chat_id}\\audio\\{name}'
        file_info = bot.get_file(id)
        downloaded_file = bot.download_file(file_info.file_path)
        bot.send_message(chat_id, 'Очаровательно, я сохраню это ')
        try:
            with open(path+'.ogg','wb') as new_file:
                new_file.write(downloaded_file)
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{name}: ', err, f' :{datetime.today()}', file=f)
        process = subprocess.run(['ffmpeg-20200211-f15007a-win64-static/bin/ffmpeg.exe', '-i', f'{path}.ogg', f'{path}.wav'])
        BotSql.appendBaseVoice(session,chat_id, f'{path}.wav', name)
        BotSql.close(session)
        os.remove(path+'.ogg')

bot.polling()
