import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
import json
from sqlalchemy.orm import sessionmaker,relationship
import os
import shutil
from datetime import datetime

Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)

class BotData(Common, Base):
    __tablename__ = "botdata"
    obj = dict()
    id_name = sa.Column(sa.Integer, unique=True, nullable=False )
    name = sa.Column(sa.String)
    audio = sa.Column(sa.JSON, default=json.dumps(obj))
    audio_ct = sa.Column(sa.Integer, default=0)
    photo = sa.Column(sa.JSON, default=json.dumps(obj))
    photo_ct = sa.Column(sa.Integer, default=0)

class Guest(Common, Base):
    __tablename__ = "guests"

    name = sa.Column(sa.String(256), nullable=False)

class Role:
    def __init__(self):
        self.engine = sa.create_engine( "sqlite:///bd_opt.sqlite?check_same_thread=False")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_schedule(self):
        return self.session.query(Speach).all()


class SpeakerRole(Role): pass

class GuestRole(Role):
    def register(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()
        self.record = guest

    def cancel(self):
        self.session.delete(self.record)
        self.session.commit()

class OrganizerRole(Role):
    def register_guest(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()

    def cancel_guest(self, id):
        guest = self.session.delete(Guest).where(Guest.id == id).all()

def actBase():
    URL = "sqlite:///botdata.sqlite"
    engine = sa.create_engine(URL)
    Base.metadata.create_all(engine)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    return session

def appendBase(session, id, fname):
    idk = (id,)
    path = os.getcwd() + f'\\users\\{id}'
    if idk not in session.query(BotData.id_name).all():
        try:
            user = BotData(id_name=id, name=fname )
            session.add(user)
            session.commit()
            os.mkdir(path)
            os.mkdir(path + '\\audio')
            os.mkdir(path + '\\photo')
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    else:
        pass

def countVoice(session, id, fname):
    appendBase(session,id, fname)
    user = session.query(BotData).filter_by(id_name=id).first()
    return(user.audio_ct)


def countPhoto(session, id, fname):
    appendBase(session,id, fname)
    user = session.query(BotData).filter_by(id_name=id).first()
    return(user.photo_ct)

def appendBaseVoice(session, id, path, fname):
    print('load voice')
    idk = (id,)
    appendBase(session, id, fname)
    user = session.query(BotData).filter_by(id_name=id).first()
    temppath = json.loads(user.audio)

    tempnum = user.audio_ct
    key = f'audio_message_{tempnum}'
    tempnum += 1
    temppath[key] = path
    user.audio = json.dumps(temppath)
    user.audio_ct = tempnum

    session.commit()

def appendBasePhoto(session, id, path, fname):
    print('load photo')
    idk = (id,)
    appendBase(session, id, fname)
    user = session.query(BotData).filter_by(id_name=id).first()
    temppath = json.loads(user.photo)

    tempnum = user.photo_ct
    key = f'photo_{tempnum}'
    tempnum += 1
    temppath[key] = path
    user.photo = json.dumps(temppath)
    user.photo_ct = tempnum

    session.commit()

def deleteBase(session, id, fname):
    appendBase(session, id, fname)
    path = os.getcwd() + f'\\users\\{id}'
    shutil.rmtree(path)
    delus = session.query(BotData).filter_by(id_name=id).first()
    session.delete(delus)
    session.commit()

def close(session):
    session.close()
